import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3 as UITK

Menu {
    id: menuActions
    
    property alias model: instantiator.model
    property bool isBottom: false
    
    function openBottom(){
        y = mainView.height - height
        isBottom = true
        open()
    }
    
    function openTop(){
        y = 0
        isBottom = false
        open()
    }
    
    Instantiator {
        id: instantiator
        
        MenuItem {
            text: modelData ? modelData.text : ""
            visible: modelData ? modelData.enabled : false
            height: visible ? Suru.units.gu(6) : 0
            indicator: UITK.Icon {
                 id: iconMenu
                 
                 implicitWidth: Suru.units.gu(3)
                 implicitHeight: implicitWidth
                 anchors.left: parent.left
                 anchors.leftMargin: Suru.units.gu(1)
                 anchors.verticalCenter: parent.verticalCenter
                 name: modelData ? modelData.iconName : ""
                 color: Suru.foregroundColor
             }
             leftPadding: iconMenu.implicitWidth + (iconMenu.anchors.leftMargin * 2)
             
             onTriggered: {
                menuActions.close()
                modelData.trigger(isBottom)
            }
        }
        onObjectAdded: menuActions.insertItem(index, object)
        onObjectRemoved: menuActions.removeItem(object)
    }
}
