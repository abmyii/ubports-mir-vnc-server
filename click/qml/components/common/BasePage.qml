import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3 as UITK

Page{
    id: basePage
    
    property list<UITK.Action> headerLeftActions
    property list<UITK.Action> headerRightActions
    property Flickable flickable
}
