import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3

Item {
    id: bottomEdgeHint
    
    signal close
    
    MouseArea{
        anchors.fill: parent
    }
    
    Rectangle {
        anchors.fill: parent
        anchors.bottomMargin: Suru.units.gu(-2)
        color: Suru.overlayColor
        opacity: 0.8
    }
    
    ColumnLayout{
        anchors{
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        Label{
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.margins: Suru.units.gu(4)

            font.pixelSize: Suru.units.gu(3)
            text: i18n.tr("Swipe from either side of the bottom edge to activate the corresponding actions in the header. \n Try it now!")
            wrapMode: Text.Wrap
            color: "white"
            horizontalAlignment: Text.AlignHCenter
        }
        
        Button{
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            
            text: i18n.tr("Okay")
            
            onClicked:{
                close()
            }
        }
    }
    
    Rectangle{
        id: edgeHint
        
        color: Suru.activeFocusColor
        height: Suru.units.gu(2)
        anchors{
            top: parent.bottom
            left: parent.left
            right: parent.right
        }
        
        PropertyAnimation { 
            id: appearAnimation
            
            running: visible
            target: edgeHint
            alwaysRunToEnd: true
            property: "opacity"
            to: 1
            duration: Suru.animations.SlowDuration
            easing: Suru.animations.EasingInOut
            onStopped: hideAnimation.start()
        }
        
        PropertyAnimation {
             id: hideAnimation
             
             target: edgeHint
             alwaysRunToEnd: true
             property: "opacity"
             to: 0
             duration: Suru.animations.SlowDuration
             easing: Suru.animations.EasingInOut
             onStopped: appearAnimation.start()
         }
    }
}
