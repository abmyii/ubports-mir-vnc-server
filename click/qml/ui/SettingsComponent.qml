import QtQuick 2.9
import QtQuick.Window 2.2
import Qt.labs.settings 1.0


Item {
    id: settingsComponent
    
    //User data
    property alias firstRun: settings.firstRun
    property alias autoRotation: settings.autoRotation
    property alias askSuspension: settings.askSuspension
    
    //VNC Settings
    property alias resolutionFraction: settings.resolutionFraction
    property alias customHeight: settings.customHeight
    property alias customWidth: settings.customWidth
    property alias refreshRateFraction: settings.refreshRateFraction
    property alias viewOnly: settings.viewOnly
    
    //App Settings
    property alias startonStartup: settings.startonStartup
    property alias alwaysShowLog: settings.alwaysShowLog
    property alias disableBacklight: settings.disableBacklight

    Settings {
        id: settings

        //User data
        property bool firstRun: true
        property bool autoRotation: true
        property bool askSuspension: true
    
        //VNC Settings
        property real resolutionFraction: 2
        property int customHeight: Screen.width / 2
        property int customWidth: Screen.height / 2
        property int refreshRateFraction: 1
        property bool viewOnly: false
        
        //App Settings
        property bool startonStartup: false
        property bool alwaysShowLog: false
        property bool disableBacklight: false
    }
}
