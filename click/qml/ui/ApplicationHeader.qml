import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Suru 2.2
import "../components/common"
import "../components/applicationheader"


ToolBar {
    id: applicationHeader
    
    readonly property real defaultHeight: Suru.units.gu(6)
    readonly property real maxHeight:  mainView.height * 0.4
    readonly property real expansionThreshold:  maxHeight * 0.50
    
    property list<BaseAction> leftActions
    property list<BaseAction> rightActions
    property Flickable flickable
    property bool expanded: false

    implicitHeight: defaultHeight
    
    function triggerRight(fromBottom){
        if (rightActions.length > 0 && rightHeaderActions.enabled) {
            if (rightActions.length === 1) {
                rightActions[0].trigger(fromBottom)
            } else {
                rightMenu.openBottom()
            }
        }
    }
    
    function triggerLeft(fromBottom) {
        if (leftActions.length === 1) {
            leftActions[0].trigger(fromBottom)
        }
    }
    
    function resetHeight() {
        if (height !== defaultHeight) {
            expanded = false
        }
    }
    
    Loader {
        id: flickableLoader
        
        active: flickable
        asynchronous: true
        sourceComponent: Connections {
            target: flickable
            
            onVerticalOvershootChanged: {
                if (target.verticalOvershoot < 0) {
                    if (applicationHeader.height < expansionThreshold) {
                        applicationHeader.height = applicationHeader.defaultHeight - target.verticalOvershoot
                    }
                    
                    if (applicationHeader.height >= expansionThreshold) {
                        expanded = true
                    }
                }
            }
            
            onContentYChanged: {
                if (target.contentY > 0 && expanded && !target.dragging) {
                    resetHeight()
                }
            }
        }
    }  
    
    NumberAnimation on height {
        id: expandAnimation
        
        running: expanded
        from: height
        to: maxHeight
        easing: Suru.animations.EasingInOut
        duration: Suru.animations.BriskDuration
    }
    
    
    NumberAnimation on height {
        id: collapseAnimation
        
        running: (!expanded || !flickable)
        from: height
        to: defaultHeight
        easing: Suru.animations.EasingInOut
        duration: Suru.animations.BriskDuration
        
        onRunningChanged: if(!running) expanded = false
    }
    
    
    RowLayout {
        spacing: Suru.units.gu(1)
        anchors.fill: parent
        
        HeaderActions{
            id: leftHeaderActions
            
            model: leftActions
            Layout.fillHeight: true
        }
        
        HeaderTitle{
            id: headerTitle
            
            text: stackView.currentItem ? stackView.currentItem.title : ""
            Layout.fillWidth: true
        }
        
        HeaderActions{
            id: rightHeaderActions
            
            model: rightActions.length === 1 ? rightActions : 0
            enabled: visible
            visible: {
                var total = 0

                for (var i = 0; i < rightActions.length - 1; i++) {
                    if (rightActions[i].enabled) {
                        total += 1
                    }
                }
                total > 0
            }

            Layout.fillHeight: true
            
            HeaderToolButton {
                id: overflowRightButton
                
                visible: rightActions ? rightActions.length > 1 : false
                iconName:  "contextual-menu"
                
                onClicked: {
                    rightMenu.openTop()
                }
                
                MenuActions{
                    id: rightMenu
                    
                    transformOrigin: Menu.TopRight
                    model: rightActions
                }
            }
        }
    }
    
    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stackView.depth > 1
        onActivated: {
            triggerLeft(false)
        }
    }

    Shortcut {
        sequence: "Menu"
        onActivated: triggerLeft(false)
    }
}
